# stega

Stega is an offline command line interface for digital text steganography.

Steganography is the art of hiding information in plain sight. Although images are usually the ideal carrier for these secrets, it is not always possible to use images in contexts such as online forums or comments.

The secret is symmetrically encrypted via AES-GCM-256 with Argon2Id for key derivation. Always use strong passwords.

**Warning**: This is a hobby project that has NOT undergone a third-party security audit.

# Installation and Usage

1. clone this repository
2. `cargo build --release`
3. `cargo test`
4. `target/release/stega -h`
