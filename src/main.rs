use std::{fs, path::PathBuf};

use aes_gcm::{
    aead::{rand_core::RngCore, Aead, KeyInit, OsRng},
    Aes256Gcm,
};
use argon2::Argon2;
use bitvec::prelude::*;
use clap::{Parser, Subcommand};
use eyre::{eyre, Context, Result};

/// AES-256-GCM with argon2 for password stretching
fn cipher(password: &str, salt: &[u8]) -> Result<Aes256Gcm> {
    let mut key: [u8; 32] = [0; 32];
    Argon2::default().hash_password_into(password.as_bytes(), &salt, &mut key)?;
    // new_from_slice requires 256-bit array despite misleading documentation
    Ok(Aes256Gcm::new_from_slice(&key)?)
}

/// salt is used as nonce so it must be 96 bits
fn encrypt(plain: &str, password: &str, salt: &[u8]) -> Result<Vec<u8>> {
    Ok(cipher(password, salt)?
        .encrypt(salt.into(), plain.as_bytes())
        .wrap_err("Failed to encrypt")?)
}

/// salt is used as nonce so it must be 96 bits
fn decrypt(ciphertext: &[u8], password: &str, salt: &[u8]) -> Result<Vec<u8>> {
    Ok(cipher(password, salt)?
        .decrypt(salt.into(), ciphertext.as_ref())
        .wrap_err("Failed to decrypt")?)
}

/// Encode given input into a String of invisible chars
fn encode(input: &[u8]) -> String {
    input
        .iter()
        .map(|byte| {
            byte.view_bits::<Lsb0>()
                .iter()
                .by_vals()
                .map(|bit| if bit { '\u{200C}' } else { '\u{200D}' })
                .collect::<Vec<char>>()
        })
        .flatten()
        .collect()
}

/// Decode given String with 0 or more invisible chars into a byte array
fn decode(input: &str) -> Vec<u8> {
    let mut bv = bitvec![u8, Lsb0;];
    for char in input.chars() {
        if char == '\u{200C}' {
            bv.push(true)
        } else if char == '\u{200D}' {
            bv.push(false)
        }
    }
    bv.into()
}

/// Encrypt and hide secret into carrier
///
/// * `carrier`: plaintext
/// * `payload`: secret to hide within plaintext
/// * `password`: password for encryption
fn hide(carrier: &str, payload: &str, password: &str) -> Result<String> {
    let mut salt = [0u8; 12];
    OsRng.fill_bytes(&mut salt);

    let mut to_hide = Vec::new();
    to_hide.extend(salt);
    to_hide.extend(encrypt(payload, password, &salt)?);

    Ok([String::from(carrier), encode(&to_hide)].join(""))
}

fn reveal(suspect: &str, password: &str) -> Result<String> {
    let decoded = decode(suspect);
    let salt = decoded
        .get(0..12)
        .ok_or_else(|| eyre!("Hidden portion too short"))?;
    let ciphertext = decoded
        .get(12..)
        .ok_or_else(|| eyre!("Hidden portion too short"))?;
    let secret = decrypt(ciphertext, password, salt)?;
    Ok(String::from_utf8(secret)?)
}

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    /// Password to encrypt secret message with
    #[arg(short, long, value_name = "PASSWORD", short = 'k')]
    password: String,

    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// Hide a message
    Hide {
        /// File with visible message to hide secret within
        carrier: PathBuf,

        /// File with message to hide.
        payload: PathBuf,
    },
    /// Reveal a message
    Reveal {
        /// File suspected to contain hidden message
        suspect: PathBuf,
    },
}

fn main() -> Result<()> {
    let cli = Cli::parse();
    match &cli.command {
        Commands::Hide { carrier, payload } => {
            let plain = fs::read_to_string(&carrier)?;
            let secret = fs::read_to_string(&payload)?;
            println!("{}", hide(&plain, &secret, &cli.password)?);
        }
        Commands::Reveal { suspect } => {
            let sus_str = fs::read_to_string(&suspect)?;
            println!(
                "{}",
                reveal(&sus_str, &cli.password).wrap_err_with(|| format!(
                    "Failed to reveal any secrets in file \"{}\"",
                    suspect.to_string_lossy()
                ))?
            );
        }
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand::distributions::{Alphanumeric, DistString};

    fn rand_string(len: usize) -> String {
        Alphanumeric.sample_string(&mut rand::thread_rng(), len)
    }

    #[test]
    fn works() {
        let carrier = rand_string(4096);
        let payload = rand_string(4096);
        let password = rand_string(16);
        assert_eq!(
            reveal(&hide(&carrier, &payload, &password).unwrap(), &password).unwrap(),
            payload
        )
    }
}
